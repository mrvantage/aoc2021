package submarine

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

type Submarine struct {
	HorizontalPosition int
	Depth              int
	Aim                int
}

func (s *Submarine) ApplyRoute(r Route) {
	for _, i := range r.Instructions {
		i.apply(s)
	}
}

func NewSubmarine(horizontal int, depth int) Submarine {
	return Submarine{horizontal, depth, 0}
}

type RouteInstructor interface {
	apply(submarine *Submarine)
}

type RouteInstruction struct {
	Amount int
}

type RouteUpInstruction struct {
	RouteInstruction
}

func (r RouteUpInstruction) apply(submarine *Submarine) {
	submarine.Depth = submarine.Depth - r.Amount
}

type RouteAimUpInstruction struct {
	RouteInstruction
}

func (r RouteAimUpInstruction) apply(submarine *Submarine) {
	submarine.Aim = submarine.Aim - r.Amount
}

type RouteDownInstruction struct {
	RouteInstruction
}

func (r RouteDownInstruction) apply(submarine *Submarine) {
	submarine.Depth = submarine.Depth + r.Amount
}

type RouteAimDownInstruction struct {
	RouteInstruction
}

func (r RouteAimDownInstruction) apply(submarine *Submarine) {
	submarine.Aim = submarine.Aim + r.Amount
}

type RouteForwardInstruction struct {
	RouteInstruction
}

func (r RouteForwardInstruction) apply(submarine *Submarine) {
	submarine.HorizontalPosition = submarine.HorizontalPosition + r.Amount
}

type RouteAimForwardInstruction struct {
	RouteInstruction
}

func (r RouteAimForwardInstruction) apply(submarine *Submarine) {
	submarine.HorizontalPosition = submarine.HorizontalPosition + r.Amount
	submarine.Depth = submarine.Depth + (r.Amount * submarine.Aim)
}

type Router interface {
	addInstruction(direction string, amount int) error
}

type Route struct {
	Instructions []RouteInstructor
}

type MainRouter struct {
	Route
}

func (m *MainRouter) addInstruction(direction string, amount int) error {
	switch direction {
	case "up":
		m.Instructions = append(m.Instructions, RouteUpInstruction{RouteInstruction{amount}})
	case "down":
		m.Instructions = append(m.Instructions, RouteDownInstruction{RouteInstruction{amount}})
	case "forward":
		m.Instructions = append(m.Instructions, RouteForwardInstruction{RouteInstruction{amount}})
	default:
		return fmt.Errorf("Invalid route direction %s", direction)
	}
	return nil
}

func NewMainRouter() MainRouter {
	return MainRouter{}
}

type AimedRouter struct {
	Route
}

func (m *AimedRouter) addInstruction(direction string, amount int) error {
	switch direction {
	case "up":
		m.Instructions = append(m.Instructions, RouteAimUpInstruction{RouteInstruction{amount}})
	case "down":
		m.Instructions = append(m.Instructions, RouteAimDownInstruction{RouteInstruction{amount}})
	case "forward":
		m.Instructions = append(m.Instructions, RouteAimForwardInstruction{RouteInstruction{amount}})
	default:
		return fmt.Errorf("Invalid route direction %s", direction)
	}
	return nil
}

func NewAimedRouter() AimedRouter {
	return AimedRouter{}
}

func LoadRouteFromInput(filename string, router Router) error {

	file, err := os.Open(filename)

	if err != nil {
		return err
	}

	s := bufio.NewScanner(file)
	s.Split(bufio.ScanLines)

	for s.Scan() {
		i := strings.Split(s.Text(), " ")
		direction := i[0]
		amount, err := strconv.Atoi(i[1])
		if err != nil {
			return err
		}

		err = router.addInstruction(direction, amount)
		if err != nil {
			return err
		}

	}

	file.Close()

	return nil
}

type Report struct {
	Input   []string
	Gamma   string
	Epsilon string
	Oxygen  string
	CO2     string
}

func (r *Report) Decode() {

	g := ""
	e := ""

	for i := 0; i < len(r.Input[0]); i++ {

		z := 0
		o := 0
		for _, l := range r.Input {
			if string(l[i]) == "0" {
				z++
			} else {
				o++
			}
		}

		if z > o {
			g = g + "0"
			e = e + "1"
		} else {
			g = g + "1"
			e = e + "0"

		}
	}

	r.Gamma = g
	r.Epsilon = e
}

func (r *Report) DecodeOxygen() {

	input := r.Input

	for i := 0; i < len(input[0]); i++ {

		z := make([]int, 0)
		o := make([]int, 0)
		for k, l := range input {
			if string(l[i]) == "0" {
				z = append(z, k)
			} else {
				o = append(o, k)
			}
		}

		inputnew := make([]string, 0)

		if len(z) > len(o) {
			for _, ii := range z {
				inputnew = append(inputnew, input[ii])
			}
		} else {
			for _, ii := range o {
				inputnew = append(inputnew, input[ii])
			}
		}

		input = inputnew

		if len(input) == 1 {
			break
		}
	}

	r.Oxygen = input[0]

}

func (r *Report) DecodeCO2() {

	input := r.Input

	for i := 0; i < len(input[0]); i++ {

		z := make([]int, 0)
		o := make([]int, 0)
		for k, l := range input {
			if string(l[i]) == "0" {
				z = append(z, k)
			} else {
				o = append(o, k)
			}
		}

		inputnew := make([]string, 0)

		if len(z) > len(o) {
			for _, ii := range o {
				inputnew = append(inputnew, input[ii])
			}
		} else {
			for _, ii := range z {
				inputnew = append(inputnew, input[ii])
			}
		}

		input = inputnew

		if len(input) == 1 {
			break
		}
	}

	r.CO2 = input[0]

}

func NewReportFromFile(filename string) (Report, error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return Report{}, err
	}

	lines := strings.Split(string(content), "\n")

	return Report{Input: lines}, nil
}

func ToDec(s string) int {

	result := 0
	base := 1
	for i := len(s) - 1; i >= 0; i-- {
		if string(s[i]) == "1" {
			result = result + base
		}
		base = base * 2
	}

	return result
}
