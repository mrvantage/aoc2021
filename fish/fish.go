package fish

import (
	"io/ioutil"
	"strconv"
	"strings"
)

type Latern struct {
	Spawn  int
	School *School
}

func (l *Latern) Itterate() {
	if l.Spawn == 0 {
		l.School.AddLatern(NewLatern(l.School, 8))
		l.Spawn = 6
	} else {
		l.Spawn--
	}
	// log.Printf("School %#v", l.School.LaternStrings())
}

func NewLatern(school *School, spawn int) Latern {
	return Latern{
		Spawn:  spawn,
		School: school,
	}
}

type School struct {
	Latern []*Latern
}

func (s *School) AddLatern(l Latern) {
	// log.Printf("Appending new latern %#v %#v", l, s.LaternStrings())
	s.Latern = append(s.Latern, &l)
	// log.Printf("Appended new latern %#v", s.LaternStrings())
}

func (s *School) Itterate() {
	// log.Printf("b: %s\n", strings.Join(s.LaternStrings(), ","))
	for _, l := range s.Latern {
		l.Itterate()
	}
	// log.Printf("a: %s\n", strings.Join(s.LaternStrings(), ","))
}

func (s *School) ItterateN(n int) {
	for i := 0; i < n; i++ {
		s.Itterate()
	}
}

func (s *School) LaternStrings() []string {
	result := make([]string, 0)
	for _, l := range s.Latern {
		result = append(result, strconv.Itoa(l.Spawn))
	}
	return result
}

func NewSchool() *School {
	return &School{
		Latern: make([]*Latern, 0),
	}
}

func NewSchoolFromFile(filename string) (*School, error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return NewSchool(), err
	}

	school := NewSchool()

	spawns := strings.Split(string(content), ",")

	for _, s := range spawns {
		spawn, err := strconv.Atoi(s)
		if err != nil {
			return NewSchool(), err
		}
		school.AddLatern(NewLatern(school, spawn))
	}

	return school, nil
}

type LaternCounter map[int]int

func (l *LaternCounter) AddLaternBySpawnTimer(timer int) {
	ll := *l
	ll[timer]++
	l = &ll
}

func (l *LaternCounter) Itterate() {

	ll := *l
	zero := ll[0]

	ll[0] = ll[1]
	ll[1] = ll[2]
	ll[2] = ll[3]
	ll[3] = ll[4]
	ll[4] = ll[5]
	ll[5] = ll[6]
	ll[6] = ll[7] + zero
	ll[7] = ll[8]
	ll[8] = zero
}

func (l *LaternCounter) ItterateN(n int) {

	for i := 0; i < n; i++ {
		l.Itterate()
	}
}

func (l *LaternCounter) Total() int {
	t := 0
	for _, n := range *l {
		t += n
	}
	return t
}

func NewLaternCounter() *LaternCounter {
	r := &LaternCounter{
		0: 0,
		1: 0,
		2: 0,
		3: 0,
		4: 0,
		5: 0,
		6: 0,
		7: 0,
		8: 0,
	}
	return r
}

func NewLaternCounterFromFile(filename string) (*LaternCounter, error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return NewLaternCounter(), err
	}

	c := NewLaternCounter()

	timers := strings.Split(string(content), ",")

	for _, i := range timers {
		timer, err := strconv.Atoi(i)
		if err != nil {
			return NewLaternCounter(), err
		}
		c.AddLaternBySpawnTimer(timer)
	}

	return c, nil
}
