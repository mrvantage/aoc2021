package sonar

import (
	"reflect"
	"testing"
)

func TestReport_NumberOfMeasurementIncreases(t *testing.T) {
	tests := []struct {
		name string
		r    *Report
		want int
	}{
		{
			"example",
			&Report{
				199,
				200,
				208,
				210,
				200,
				207,
				240,
				269,
				260,
				263,
			},
			7,
		},
		{
			"empty report",
			&Report{},
			0,
		},
		{
			"5 increases",
			&Report{
				0,
				1,
				2,
				3,
				4,
				5,
			},
			5,
		},
		{
			"5 decreases",
			&Report{
				5,
				4,
				3,
				2,
				1,
				0,
			},
			0,
		},
		{
			"negative increases",
			&Report{
				-5,
				-4,
				-3,
				-2,
				-1,
				0,
			},
			5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.r.NumberOfMeasurementIncreases(); got != tt.want {
				t.Errorf("Report.NumberOfMeasurementIncreases() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewReportFromInput(t *testing.T) {
	type args struct {
		filename string
	}
	tests := []struct {
		name    string
		args    args
		want    Report
		wantErr bool
	}{
		{
			"file does not exist",
			args{
				"non-existing-file",
			},
			Report{},
			true,
		},
		{
			"simple",
			args{
				"sonar_test_simple.txt",
			},
			Report{0, 1, 2, 3, 4, 5},
			false,
		},
		{
			"invalid",
			args{
				"sonar_test_invalid.txt",
			},
			Report{},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewReportFromInput(tt.args.filename)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewReportFromInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewReportFromInput() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestReport_NumberOfWindowedMeasurementIncreases(t *testing.T) {
	type args struct {
		windowsize int
	}
	tests := []struct {
		name string
		r    *Report
		args args
		want int
	}{
		{
			"example",
			&Report{
				199,
				200,
				208,
				210,
				200,
				207,
				240,
				269,
				260,
				263,
			},
			args{3},
			5,
		},
		{
			"empty report",
			&Report{},
			args{3},
			0,
		},
		{
			"5 increasing measurements",
			&Report{
				0,
				1,
				2,
				3,
				4,
				5,
			},
			args{3},
			3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.r.NumberOfWindowedMeasurementIncreases(tt.args.windowsize); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Report.getWindowSumsAsReport() = %v, want %v", got, tt.want)
			}
		})
	}
}
