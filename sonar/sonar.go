package sonar

import (
	"bufio"
	"os"
	"strconv"
)

type Report []int

func (r *Report) NumberOfMeasurementIncreases() int {
	result := 0
	report := *r
	for k, v := range report {
		if k > 0 {
			if v > report[k-1] {
				result++
			}
		}
	}
	return result
}

func (r *Report) NumberOfWindowedMeasurementIncreases(windowsize int) int {
	sums := Report{}
	report := *r

	start := 0
	end := start + windowsize

	for end <= len(report) {
		s := 0
		for _, v := range report[start:end] {
			s = s + v
		}
		sums = append(sums, s)
		start++
		end++
	}

	return sums.NumberOfMeasurementIncreases()
}

func NewReportFromInput(filename string) (Report, error) {

	file, err := os.Open(filename)

	if err != nil {
		return Report{}, err
	}

	s := bufio.NewScanner(file)
	s.Split(bufio.ScanLines)

	var result Report
	for s.Scan() {
		m, err := strconv.Atoi(s.Text())
		if err != nil {
			return Report{}, err
		}
		result = append(result, m)
	}

	file.Close()

	return result, nil
}
