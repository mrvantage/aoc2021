package bingo

import (
	"io/ioutil"
	"strconv"
	"strings"
)

type Board struct {
	Fields       [][]int
	FieldsCalled [][]bool
}

func (b *Board) CallNumber(n int) {
	for colNum, column := range b.Fields {
		for rowNum, field := range column {
			if field == n {
				b.FieldsCalled[colNum][rowNum] = true
			}
		}
	}

	// fmt.Printf("Number %d called: %#v\n", n, b)
}

func (b *Board) CheckCol(colNum int) bool {
	result := true
	for _, v := range b.FieldsCalled[colNum] {
		if !v {
			result = false
		}
	}
	return result
}

func (b *Board) CheckRow(rowNum int) bool {
	result := true
	for _, col := range b.FieldsCalled {
		if !col[rowNum] {
			result = false
		}
	}
	// fmt.Printf("Checked row %d to be %t: %#v\n", rowNum, result, b)
	return result
}

func (b *Board) WinsLine() bool {
	for i := 0; i < len(b.Fields); i++ {
		if b.CheckCol(i) || b.CheckRow(i) {
			return true
		}
	}
	return false
}

func (b *Board) Score() int {
	score := 0
	for colNum, column := range b.Fields {
		for rowNum, field := range column {
			if !b.FieldsCalled[colNum][rowNum] {
				score = score + field
			}
		}
	}
	return score
}

func NewBoard(size int) Board {
	fields := make([][]int, 5)
	for i := 0; i < size; i++ {
		fields[i] = make([]int, size)
	}

	fieldsCalled := make([][]bool, 5)
	for i := 0; i < size; i++ {
		fieldsCalled[i] = make([]bool, size)
	}

	return Board{
		Fields:       fields,
		FieldsCalled: fieldsCalled,
	}
}

func NewBoardFromInput(input string) (Board, error) {
	lines := strings.Split(input, "\n")

	result := NewBoard(len(lines))

	for lineNum, line := range lines {
		fields := strings.Fields(line)
		for fieldNum, field := range fields {
			n, err := strconv.Atoi(field)
			if err != nil {
				return Board{}, err
			}
			result.Fields[fieldNum][lineNum] = n
		}
	}
	return result, nil
}

type Game struct {
	Draws  []int
	Boards []Board
}

func (g *Game) CallNumber(n int) {
	for _, board := range g.Boards {
		board.CallNumber(n)
	}
}

func (g *Game) WinsLine() int {
	for i, board := range g.Boards {
		if board.WinsLine() {
			return i
		}
	}
	return -1
}

func (g *Game) CheckDrawsForLine() (int, int) {
	for drawNum, n := range g.Draws {
		g.CallNumber(n)
		boardNum := g.WinsLine()
		if boardNum > -1 {
			return boardNum, drawNum
		}
	}

	return -1, -1
}

func (g *Game) RemoveBoard(boardNum int) {
	g.Boards = append(g.Boards[:boardNum], g.Boards[(boardNum+1):]...)
}

func (g *Game) GetLastWinningBoard() (int, int) {

	for drawNum, n := range g.Draws {
		// fmt.Printf("Drawing number: %d\n", n)
		g.CallNumber(n)
		for boardNum := g.WinsLine(); boardNum > -1; boardNum = g.WinsLine() {
			// fmt.Printf("Board %d won! Number of boards: %d\n", boardNum, len(g.Boards))

			if len(g.Boards) == 1 {
				return boardNum, drawNum
			} else {
				g.RemoveBoard(boardNum)
			}
		}
	}

	return -1, -1
}

func NewGameFromInput(input string) (Game, error) {
	components := strings.Split(input, "\n\n")
	drawsStr := strings.Split(components[0], ",")
	draws := make([]int, 0)

	for _, dStr := range drawsStr {
		dInt, err := strconv.Atoi(dStr)
		if err != nil {
			return Game{}, err
		}
		draws = append(draws, dInt)
	}

	boards := make([]Board, 0)
	for _, b := range components[1:] {
		board, err := NewBoardFromInput(b)
		if err != nil {
			return Game{}, err
		}
		boards = append(boards, board)
	}

	return Game{
		Draws:  draws,
		Boards: boards,
	}, nil
}

func NewGameFromFile(filename string) (Game, error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return Game{}, err
	}

	game, err := NewGameFromInput(string(content))
	if err != nil {
		return Game{}, err
	}

	return game, nil
}
