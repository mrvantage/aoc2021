package vent

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type Coordinate struct {
	X int
	Y int
}

type Line struct {
	Start Coordinate
	End   Coordinate
}

func (l *Line) IsVertical() bool {
	return l.Start.X == l.End.X
}

func (l *Line) IsHorizontal() bool {
	return l.Start.Y == l.End.Y
}

func (l *Line) IsDiagonal() bool {
	return AbsInt(l.End.X-l.Start.X) == AbsInt(l.End.Y-l.Start.Y)
}

func (l *Line) LengthX() int {
	return AbsInt(l.Start.X-l.End.X) + 1
}

func (l *Line) LengthY() int {
	//log.Printf("Length Y=%d", AbsInt(l.Start.Y-l.End.Y)+1)
	return AbsInt(l.Start.Y-l.End.Y) + 1
}

func (l *Line) MinX() int {
	if l.Start.X < l.End.X {
		return l.Start.X
	} else {
		return l.End.X
	}
}

func (l *Line) MinY() int {
	if l.Start.Y < l.End.Y {
		//log.Printf("Min Y=%d", l.Start.Y)
		return l.Start.Y
	} else {
		//log.Printf("Min Y=%d", l.End.Y)
		return l.End.Y
	}
}

func (l *Line) GetCoordinates() []Coordinate {
	result := make([]Coordinate, 0)

	if l.IsHorizontal() {
		for i := l.MinX(); i < l.LengthX()+l.MinX(); i++ {
			//log.Printf("Adding for X=%d", i)
			result = append(result, Coordinate{X: i, Y: l.Start.Y})
		}
	} else if l.IsVertical() {
		for i := l.MinY(); i < l.LengthY()+l.MinY(); i++ {
			//log.Printf("Adding for Y=%d", i)
			result = append(result, Coordinate{X: l.Start.X, Y: i})
		}
	} else {

		h := 1
		if l.End.X < l.Start.X {
			h = -1
		}

		v := 1
		if l.End.Y < l.Start.Y {
			v = -1
		}

		for i := 0; i < l.LengthY(); i++ {
			result = append(result, Coordinate{X: l.Start.X + (h * i), Y: l.Start.Y + (v * i)})
		}
	}

	return result
}

type Lines []Line

func (l *Lines) GetMaxXY() (int, int) {
	maxX := 0
	maxY := 0
	for _, line := range *l {
		if line.Start.X > maxX {
			maxX = line.Start.X
		}
		if line.Start.Y > maxY {
			maxY = line.Start.Y
		}

		if line.End.X > maxX {
			maxX = line.End.X
		}
		if line.End.Y > maxY {
			maxY = line.End.Y
		}
	}
	return maxX, maxY
}

func NewLinesFromFile(filename string) (Lines, error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return Lines{}, err
	}

	lines := make(Lines, 0)

	for _, l := range strings.Split(string(content), "\n") {
		c := strings.Split(l, " -> ")
		s := strings.Split(c[0], ",")
		sx, err := strconv.Atoi(s[0])
		if err != nil {
			return Lines{}, err
		}
		sy, err := strconv.Atoi(s[1])
		if err != nil {
			return Lines{}, err
		}
		e := strings.Split(c[1], ",")
		ex, err := strconv.Atoi(e[0])
		if err != nil {
			return Lines{}, err
		}
		ey, err := strconv.Atoi(e[1])
		if err != nil {
			return Lines{}, err
		}

		lines = append(
			lines,
			Line{
				Start: Coordinate{
					X: sx,
					Y: sy,
				},
				End: Coordinate{
					X: ex,
					Y: ey,
				},
			},
		)
	}

	return lines, nil
}

type Field struct {
	Lines  Lines
	Points [][]int
}

func (f *Field) ApplyLine(l Line) {
	//log.Printf("Applying line %#v\n", l)
	for _, c := range l.GetCoordinates() {
		f.Points[c.X][c.Y]++
	}
}

func (f *Field) ApplyLines(horizontal bool, vertical bool, diagonal bool) {
	for _, l := range f.Lines {
		if horizontal && l.IsHorizontal() {
			f.ApplyLine(l)
		} else if vertical && l.IsVertical() {
			f.ApplyLine(l)
		} else if diagonal && l.IsDiagonal() {
			f.ApplyLine(l)
		}

	}
}

func (f *Field) Print() {
	width, height := f.Lines.GetMaxXY()

	lines := make([]string, 0)

	for y := 0; y <= height; y++ {
		line := ""
		for x := 0; x <= width; x++ {
			if f.Points[x][y] == 0 {
				line = line + "."
			} else {
				line = line + fmt.Sprintf("%d", f.Points[x][y])
			}
		}
		lines = append(lines, line)
	}

	fmt.Printf("%s\n", strings.Join(lines, "\n"))
}

func (f *Field) GetNumberOfPointsAboveThreshold(threshold int) int {
	width, height := f.Lines.GetMaxXY()

	result := 0

	for y := 0; y <= height; y++ {
		for x := 0; x <= width; x++ {
			if f.Points[x][y] > threshold {
				result++
			}
		}
	}

	return result
}

func NewFieldFromLines(lines Lines) Field {
	width, height := lines.GetMaxXY()

	points := make([][]int, width+1)
	for i, _ := range points {
		points[i] = make([]int, height+1)
	}

	return Field{
		Lines:  lines,
		Points: points,
	}
}

func AbsInt(i int) int {
	if i < 0 {
		return -i
	} else {
		return i
	}
}
