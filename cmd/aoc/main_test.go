package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
	"testing"
)

const (
	fixturesDir = "testdata"
	testTmp     = "tmp"
	// SubCmdFlags space separated list of command line flags.
	TestMainArgs = "TEST_MAIN_ARGS"
)

var logFile *os.File

func Test_main(t *testing.T) {

	// Call main when this is a sub process to actually call main()
	callMain()

	tests := []struct {
		name string
		args []string
		want struct {
			exitCode int
			stdout   string
			// stderr   string
		}
	}{
		{
			name: "d1p1",
			args: []string{
				"-file",
				"../../input_01.txt",
				"d1p1",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "1527\n",
			},
		},
		{
			name: "d1p2",
			args: []string{
				"-file",
				"../../input_01.txt",
				"d1p2",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "1575\n",
			},
		},
		{
			name: "d2p1",
			args: []string{
				"-file",
				"../../input_02.txt",
				"d2p1",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "1692075\n",
			},
		},
		{
			name: "d2p2",
			args: []string{
				"-file",
				"../../input_02.txt",
				"d2p2",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "1749524700\n",
			},
		},
		{
			name: "d3p1",
			args: []string{
				"-file",
				"../../input_03.txt",
				"d3p1",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "852500\n",
			},
		},
		{
			name: "d3p2",
			args: []string{
				"-file",
				"../../input_03.txt",
				"d3p2",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "1007985\n",
			},
		},
		{
			name: "d4p1",
			args: []string{
				"-file",
				"../../input_04.txt",
				"d4p1",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "33462\n",
			},
		},
		{
			name: "d4p2",
			args: []string{
				"-file",
				"../../input_04.txt",
				"d4p2",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "30070\n",
			},
		},
		{
			name: "d5p1",
			args: []string{
				"-file",
				"../../input_05.txt",
				"d5p1",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "5698\n",
			},
		},
		{
			name: "d5p2",
			args: []string{
				"-file",
				"../../input_05.txt",
				"d5p2",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "15463\n",
			},
		},
		{
			name: "d6p1",
			args: []string{
				"-file",
				"../../input_06.txt",
				"d6p1",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "345387\n",
			},
		},
		{
			name: "d6p2",
			args: []string{
				"-file",
				"../../input_06.txt",
				"d6p2",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "1574445493136\n",
			},
		},
		{
			name: "d7p1",
			args: []string{
				"-file",
				"../../input_07.txt",
				"d7p1",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "342534\n",
			},
		},
		{
			name: "d7p2",
			args: []string{
				"-file",
				"../../input_07.txt",
				"d7p2",
			},
			want: struct {
				exitCode int
				stdout   string
			}{
				exitCode: 0,
				stdout:   "94004208\n",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cmd := getMainSubCommand(t.Name(), tt.args)
			fmt.Printf("%#v\n", cmd)

			// Execute the subcommand
			stdout, cmdError := cmd.CombinedOutput()
			fmt.Printf("%#v\n%#v\n", string(stdout), cmdError)

			// get exit code.
			exitCode := cmd.ProcessState.ExitCode()

			if exitCode != tt.want.exitCode {
				t.Errorf("got exitCode %v, want %v", exitCode, tt.want.exitCode)
			}

			if string(stdout) != tt.want.stdout {
				t.Errorf("got stdout %#v, want %#v", string(stdout), tt.want.stdout)
			}

			if cmdError != nil {
				fmt.Printf("\nBEGIN sub-command\nstdout:\n%v\n\n", string(stdout))
				fmt.Printf("stderr:\n%v\n", cmdError.Error())
				fmt.Print("\nEND sub-command\n\n")
			}
		})
	}
}

func callMain() {
	// Check if TEST_MAIN_ARGS is set in environment
	// If so the this process is triggered from the actual test
	// and we may call main()
	if os.Getenv(TestMainArgs) != "" {

		// Prepare environment before calling main()
		args := strings.Split(os.Getenv(TestMainArgs), " ")
		os.Args = append([]string{os.Args[0]}, args...)

		// Call main()
		main()
		os.Exit(0)
	}
}

func getMainSubCommand(testFunc string, args []string) *exec.Cmd {
	// Create a subcommand that will call main() for us.
	cmd := exec.Command(os.Args[0], "-test.run", testFunc)

	// Set the args to be injected by the subprocess before calling main()
	// This is communicated via the environment.
	env := TestMainArgs + "=" + strings.Join(args, " ")
	// cmd.Env = []string{env}
	cmd.Env = append(os.Environ(), env)

	return cmd
}
