package main

import (
	"flag"
	"fmt"
	"log"

	"gitlab.com/mrvantage/aoc2021/bingo"
	"gitlab.com/mrvantage/aoc2021/fish"
	"gitlab.com/mrvantage/aoc2021/sonar"
	"gitlab.com/mrvantage/aoc2021/submarine"
	"gitlab.com/mrvantage/aoc2021/vent"
	"gitlab.com/mrvantage/aoc2021/whale"
)

type puzzleFunc func(input string)

var puzzles map[string]puzzleFunc

func main() {

	flag.CommandLine.Usage = usage

	puzzles = map[string]puzzleFunc{
		"d1p1": d1p1,
		"d1p2": d1p2,
		"d2p1": d2p1,
		"d2p2": d2p2,
		"d3p1": d3p1,
		"d3p2": d3p2,
		"d4p1": d4p1,
		"d4p2": d4p2,
		"d5p1": d5p1,
		"d5p2": d5p2,
		"d6p1": d6p1,
		"d6p2": d6p2,
		"d7p1": d7p1,
		"d7p2": d7p2,
	}

	var input string

	// flag.StringVar(&puzzle, "puzzle", "", "Which puzzle to run, two digits for the day, followed by a 1 or 2 to specify which puzzle.")
	flag.StringVar(&input, "file", "", "Path to the file containing the input for the puzzle")
	flag.Parse()

	puzzle := flag.Arg(0)

	if f, exists := puzzles[puzzle]; exists {
		f(input)
	} else {
		fmt.Printf("No such action %s\n", puzzle)
	}

}

func usage() {

	if flag.CommandLine.Name() == "" {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage:\n")
	} else {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", flag.CommandLine.Name())
	}

	fmt.Println("\naoc [global options] <puzzle> [puzzle options]")

	fmt.Println("\npuzzle:")
	for puzzle := range puzzles {
		fmt.Printf("  %s\n", puzzle)
	}

	fmt.Println("\nuse [puzzle] -help to see the possible action options.")

	fmt.Println("\nglobal options:")
	flag.CommandLine.PrintDefaults()
}

func d1p1(input string) {
	r, _ := sonar.NewReportFromInput(input)
	fmt.Printf("%d\n", r.NumberOfMeasurementIncreases())
}

func d1p2(input string) {

	f := flag.NewFlagSet("d1p2", flag.ExitOnError)

	var window int
	f.IntVar(&window, "window", 3, "Rolling window size")
	f.Parse(flag.Args()[1:])

	r, _ := sonar.NewReportFromInput(input)
	fmt.Printf("%d\n", r.NumberOfWindowedMeasurementIncreases(window))
}

func d2p1(input string) {
	s := submarine.NewSubmarine(0, 0)
	r := submarine.NewMainRouter()
	err := submarine.LoadRouteFromInput(input, &r)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}
	s.ApplyRoute(r.Route)
	fmt.Printf("%d\n", (s.HorizontalPosition * s.Depth))
}

func d2p2(input string) {
	s := submarine.NewSubmarine(0, 0)
	r := submarine.NewAimedRouter()
	err := submarine.LoadRouteFromInput(input, &r)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}

	s.ApplyRoute(r.Route)

	fmt.Printf("%d\n", (s.HorizontalPosition * s.Depth))
}

func d3p1(input string) {
	r, err := submarine.NewReportFromFile(input)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}

	r.Decode()

	g := submarine.ToDec(r.Gamma)
	e := submarine.ToDec(r.Epsilon)

	fmt.Printf("%d\n", g*e)
}

func d3p2(input string) {
	r, err := submarine.NewReportFromFile(input)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}

	r.DecodeOxygen()
	r.DecodeCO2()

	o := submarine.ToDec(r.Oxygen)
	c := submarine.ToDec(r.CO2)

	fmt.Printf("%d\n", o*c)
}

func d4p1(input string) {
	g, err := bingo.NewGameFromFile(input)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}

	boardNum, drawNum := g.CheckDrawsForLine()

	boardScore := g.Boards[boardNum].Score()

	totalScore := boardScore * g.Draws[drawNum]

	fmt.Printf("%d\n", totalScore)
}

func d4p2(input string) {
	g, err := bingo.NewGameFromFile(input)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}

	boardNum, drawNum := g.GetLastWinningBoard()

	var boardScore, totalScore int
	boardScore = g.Boards[boardNum].Score()

	totalScore = boardScore * g.Draws[drawNum]

	fmt.Printf("%d\n", totalScore)

}

func d5p1(input string) {

	fs := flag.NewFlagSet("d5p1", flag.ExitOnError)

	var print bool
	fs.BoolVar(&print, "print", false, "Print vent matrix to stdout")
	fs.Parse(flag.Args()[1:])

	l, err := vent.NewLinesFromFile(input)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}

	f := vent.NewFieldFromLines(l)

	f.ApplyLines(true, true, false)

	if print {
		f.Print()
	}

	fmt.Printf("%d\n", f.GetNumberOfPointsAboveThreshold(1))
}

func d5p2(input string) {

	fs := flag.NewFlagSet("d5p1", flag.ExitOnError)

	var print bool
	fs.BoolVar(&print, "print", false, "Print vent matrix to stdout")
	fs.Parse(flag.Args()[1:])

	l, err := vent.NewLinesFromFile(input)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}

	f := vent.NewFieldFromLines(l)

	f.ApplyLines(true, true, true)

	if print {
		f.Print()
	}

	fmt.Printf("%d\n", f.GetNumberOfPointsAboveThreshold(1))
}

func d6p1(input string) {

	f := flag.NewFlagSet("d6p1", flag.ExitOnError)

	var itterations int
	f.IntVar(&itterations, "itterations", 80, "Rolling window size")
	f.Parse(flag.Args()[1:])

	s, err := fish.NewSchoolFromFile(input)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}

	s.ItterateN(itterations)

	fmt.Printf("%d\n", len(s.Latern))

}

func d6p2(input string) {

	f := flag.NewFlagSet("d6p2", flag.ExitOnError)

	var itterations int
	f.IntVar(&itterations, "itterations", 256, "Rolling window size")
	f.Parse(flag.Args()[1:])

	c, err := fish.NewLaternCounterFromFile(input)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}

	c.ItterateN(itterations)

	fmt.Printf("%d\n", c.Total())

}

func d7p1(input string) {

	c, err := whale.LoadInput(input)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}

	fmt.Printf("%d\n", whale.FindPoint(c))

}

func d7p2(input string) {

	c, err := whale.LoadInput(input)
	if err != nil {
		log.Fatalf("Failed loading file: %s", err)
	}

	fmt.Printf("%d\n", whale.FindPoint2(c))

}
