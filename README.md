# Advent of Code 2021

Run code for a puzzle:
```
go run ./cmd/01-1/main.go ./cmd/01-1/input.txt
go run ./cmd/01-2/main.go ./cmd/01-2/input.txt 3
```

Run unit tests:
```
go test --cover ./...
```