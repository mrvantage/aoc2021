package whale

import (
	"io/ioutil"
	"strconv"
	"strings"

	"gitlab.com/mrvantage/aoc2021/vent"
)

func FindPoint(i []int) int {
	min := 0
	foundMin := false
	max := 0
	for _, v := range i {
		if v > max {
			max = v
		}
		if v < min || !foundMin {
			min = v
			foundMin = true
		}
	}

	minF := 0
	foundF := false
	for p := min; p <= max; p++ {
		f := 0
		for _, c := range i {
			f += vent.AbsInt(p - c)
		}

		if f < minF || !foundF {
			minF = f
			foundF = true
		}
	}
	return minF
}

func FindPoint2(i []int) int {
	min := 0
	foundMin := false
	max := 0
	for _, v := range i {
		if v > max {
			max = v
		}
		if v < min || !foundMin {
			min = v
			foundMin = true
		}
	}

	minF := 0
	foundF := false
	for p := min; p <= max; p++ {
		f := 0
		for _, c := range i {
			m := vent.AbsInt(p - c)

			for i := 1; i <= m; i++ {
				f += i
			}
		}

		if f < minF || !foundF {
			minF = f
			foundF = true
		}
	}
	return minF
}

func LoadInput(filename string) ([]int, error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return []int{}, err
	}

	result := make([]int, 0)
	for _, l := range strings.Split(string(content), ",") {
		i, err := strconv.Atoi(l)
		if err != nil {
			return []int{}, err
		}

		result = append(result, i)
	}

	return result, nil
}
